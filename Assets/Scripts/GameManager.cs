﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    List<Combat> combatList = new List<Combat>();
    // Start is called before the first frame update
    void Start()
    {
        combatList.Add(new Combat(1, "Bulbasaur", "Mewtwo"));
        combatList.Add(new Combat(2, "Pikachu", "Terrakion"));
        Debug.Log(countCombats("Mewtwo"));
    }

    // Update is called once per frame
    void Update()
    {

    }
    int countCombats(string pokemon)
    {
        int nCombats = 0;

        for (int i = 0; i < combatList.Count; i++)
        {
            if (combatList[i].pokemonName1.Equals(pokemon) || combatList[i].pokemonName2.Equals(pokemon))
            {
                nCombats++;
            }
        }
        return nCombats;

    }
    void setWinner(int id, string pokemon) {
        for (int i=0; i<combatList.Count; i++) {
            if (combatList[i].pokemonName1.Equals(pokemon)|| combatList[i].pokemonName2.Equals(pokemon)) {
                combatList[i].pokemonWinner = pokemon;
            }        
        }
    }
   
}
