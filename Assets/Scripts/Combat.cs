﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    public int id;
    public string pokemonName1;
    public string pokemonName2;

    public string pokemonWinner;

    public Combat (int id, string pokemonName1, string pokemonName2)
    {
        this.id = id;
        this.pokemonName1 = pokemonName1;
        this.pokemonName2 = pokemonName2;
      
    }

    public Combat() { }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    string getWinner() {
        return this.pokemonWinner;
    }

}